import unittest
from reg_check_phone import check_phone_app
import re

class TestPhone(unittest.TestCase):
    def setUp(self):
        self.test_check_phone = check_phone_app()

    def test_reg_check_phone(self):
        self.assertTrue(self.test_reg_check_phone.check_num('9138885467'))
        self.assertTrue(self.test_reg_check_phone.check_num('9234382926'))
        self.assertFalse(self.test_reg_check_phone.check_num('8-999-784-95-95'))
        self.assertFalse(self.test_reg_check_phone.check_num('8-916-000-00-00'))
        self.assertFalse(self.test_reg_check_phone.check_num('9160000000'))
        self.assertFalse(self.test_reg_check_phone.check_num('4521'))

