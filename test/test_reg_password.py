import unittest
from reg_check_password import check_password_app
import re

class TestPassword(unittest.TestCase):
    def setUp(self):
        self.test_check_password = check_password_app()

    def test_reg_check_password(self):
        self.assertTrue(self.test_reg_check_password.check_num('p2Vl9AEST'))
        self.assertTrue(self.test_reg_check_password.check_num('9G1iuoiUe'))
        self.assertTrue(self.test_reg_check_password.check_num('9Zk6aRXFl'))
        self.assertTrue(self.test_reg_check_password.check_num('1y5AMBWCO'))
        self.assertFalse(self.test_reg_check_password.check_num('12345678910'))
        self.assertFalse(self.test_reg_check_password.check_num('dJBnnuojQ'))
        self.assertFalse(self.test_reg_check_password.check_num('PXSPXMXJK'))
        self.assertFalse(self.test_reg_check_password.check_num('9ntmd01hj'))


