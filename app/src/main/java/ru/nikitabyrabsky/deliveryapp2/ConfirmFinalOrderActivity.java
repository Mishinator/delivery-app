package ru.nikitabyrabsky.deliveryapp2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import ru.nikitabyrabsky.deliveryapp2.Prevalant.Prevalent;

public class ConfirmFinalOrderActivity extends AppCompatActivity
{

    private EditText nameEditText, phoneEditText, addressEditText, cityEditText, timeEditText, dateEditText;
    private Button confirmOrderBtn;
    private Button cashBtn, terminalBtn;
    private String wayPay;

    private ProgressBar progressBar;

    private String totalAmount = "";


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_final_order);


        totalAmount = getIntent().getStringExtra("Итоговая сумма");
        Toast.makeText(this, "Итоговая сумма = " + totalAmount + " ₽", Toast.LENGTH_SHORT).show();


        confirmOrderBtn = (Button)findViewById(R.id.confirm_final_order_btn);
        nameEditText = (EditText)findViewById(R.id.delivery_name);
        phoneEditText = (EditText)findViewById(R.id.delivery_phone_number);
        addressEditText = (EditText)findViewById(R.id.delivery_address);
        cityEditText = (EditText)findViewById(R.id.delivery_city);

        dateEditText = (EditText)findViewById(R.id.delivery_date);
        timeEditText = (EditText)findViewById(R.id.delivery_time);

        cashBtn = (Button)findViewById(R.id.pay_by_cash_btn);
        terminalBtn = (Button)findViewById(R.id.pay_by_terminal_btn);

        progressBar = (ProgressBar)findViewById(R.id.progressBar3);

        cashBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                wayPay = "наличные";
                cashBtn.setBackgroundResource(R.color.cash);
                terminalBtn.setBackgroundResource(R.color.terminal);
            }
        });

        terminalBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                wayPay = "терминал";
                cashBtn.setBackgroundResource(R.color.terminal);
                terminalBtn.setBackgroundResource(R.color.cash);
            }
        });





        confirmOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Check();
            }
        });
    }


    private void Check()
    {
        if(TextUtils.isEmpty(nameEditText.getText().toString()))
        {
            Toast.makeText(this, "Пожалуйста, введите ваше имя", Toast.LENGTH_SHORT).show();
        }
        else if(TextUtils.isEmpty(phoneEditText.getText().toString()))
        {
            Toast.makeText(this, "Пожалуйста, введите ваш номер телефона", Toast.LENGTH_SHORT).show();
        }
        else if(TextUtils.isEmpty(cityEditText.getText().toString()))
        {
            Toast.makeText(this, "Пожалуйста, введите ваш город", Toast.LENGTH_SHORT).show();
        }
        else if(TextUtils.isEmpty(addressEditText.getText().toString()))
        {
            Toast.makeText(this, "Пожалуйста, введите адрес доставки", Toast.LENGTH_SHORT).show();
        }
        else if(TextUtils.isEmpty(dateEditText.getText().toString()))
        {
            Toast.makeText(this, "Пожалуйста, введите день доставки", Toast.LENGTH_SHORT).show();
        }
        else if(TextUtils.isEmpty(timeEditText.getText().toString()))
        {
            Toast.makeText(this, "Пожалуйста, введите удобное время доставки", Toast.LENGTH_SHORT).show();
        }
        else if(wayPay != "наличные" && wayPay != "терминал")
        {
            Toast.makeText(this, "Пожалуйста, выберите способ оплаты", Toast.LENGTH_SHORT).show();
        }
        else
        {
            ConfirmOrder();
        }
    }

    private void ConfirmOrder()
    {

        progressBar.setVisibility(ProgressBar.VISIBLE);

        final String saveCurrentDate, saveCurrentTime;

        Calendar calForDate = Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("MMM dd, yyyy");
        saveCurrentDate = currentDate.format(calForDate.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("H:mm a");
        saveCurrentTime = currentTime.format(calForDate.getTime());

        final DatabaseReference ordersRef = FirebaseDatabase.getInstance().getReference()
                .child("Orders")
                .child(Prevalent.currentOnlineUser.getPhone());

        HashMap<String, Object> ordersMap = new HashMap<>();
        ordersMap.put("totalAmount", totalAmount);
        ordersMap.put("user_name", nameEditText.getText().toString());
        ordersMap.put("phone", phoneEditText.getText().toString());
        ordersMap.put("city", cityEditText.getText().toString());
        ordersMap.put("address", addressEditText.getText().toString());
        ordersMap.put("date", saveCurrentDate);
        ordersMap.put("time", saveCurrentTime);
        ordersMap.put("delivery_time", timeEditText.getText().toString());
        ordersMap.put("delivery_date", dateEditText.getText().toString());
        ordersMap.put("payway", wayPay);
        ordersMap.put("state", "nothing yet");

        ordersRef.updateChildren(ordersMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task)
            {
                if(task.isSuccessful())
                {
                    ///при оформлении заказа, корзина очищается
                    FirebaseDatabase.getInstance().getReference().child("Cart List")
                            .child("User View")
                            .child(Prevalent.currentOnlineUser.getPhone())
                            .removeValue()
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task)
                                {
                                    if(task.isSuccessful())
                                    {
                                        Toast.makeText(ConfirmFinalOrderActivity.this, "Ваш заказ успешно сформирован!", Toast.LENGTH_SHORT).show();

                                        Intent intent = new Intent(ConfirmFinalOrderActivity.this, MenuActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        finish();
                                        progressBar.setVisibility(ProgressBar.INVISIBLE);
                                    }
                                }
                            });
                }
            }
        });

    }
}