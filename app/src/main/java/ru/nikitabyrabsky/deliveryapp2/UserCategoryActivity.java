package ru.nikitabyrabsky.deliveryapp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class UserCategoryActivity extends AppCompatActivity
{

    private ImageView fruit_vegetables, grocery, bakery_products;
    private ImageView canned_food, meat_products, milk_products;
    private ImageView tea_and_coffee, water, household_chemicals;
    private ImageView animals_products, frozen_products, sweet_products;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_category);


        fruit_vegetables = (ImageView)findViewById(R.id.user_fruit_vegetables);
        grocery = (ImageView)findViewById(R.id.user_grocery);
        bakery_products = (ImageView)findViewById(R.id.user_bakery_products);

        canned_food = (ImageView)findViewById(R.id.user_canned_food);
        meat_products = (ImageView)findViewById(R.id.user_meat_products);
        milk_products = (ImageView)findViewById(R.id.user_milk_products);

        tea_and_coffee = (ImageView)findViewById(R.id.user_tea_and_coffee);
        water = (ImageView)findViewById(R.id.user_water);
        household_chemicals = (ImageView)findViewById(R.id.user_household_chemicals);

        animals_products = (ImageView)findViewById(R.id.user_animals_products);
        frozen_products = (ImageView)findViewById(R.id.user_frozen_products);
        sweet_products = (ImageView)findViewById(R.id.user_sweet_products);


        fruit_vegetables.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(UserCategoryActivity.this, ProductsByCategoriesActivity.class);
                intent.putExtra("category", "fruit_vegetables");
                startActivity(intent);
            }
        });


        grocery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(UserCategoryActivity.this, ProductsByCategoriesActivity.class);
                intent.putExtra("category", "grocery");
                startActivity(intent);
            }
        });


        bakery_products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(UserCategoryActivity.this, ProductsByCategoriesActivity.class);
                intent.putExtra("category", "bakery_products");
                startActivity(intent);
            }
        });


        canned_food.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(UserCategoryActivity.this, ProductsByCategoriesActivity.class);
                intent.putExtra("category", "canned_food");
                startActivity(intent);
            }
        });


        meat_products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(UserCategoryActivity.this, ProductsByCategoriesActivity.class);
                intent.putExtra("category", "meat_products");
                startActivity(intent);
            }
        });


        milk_products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(UserCategoryActivity.this, ProductsByCategoriesActivity.class);
                intent.putExtra("category", "milk_products");
                startActivity(intent);
            }
        });


        tea_and_coffee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(UserCategoryActivity.this, ProductsByCategoriesActivity.class);
                intent.putExtra("category", "tea_and_coffee");
                startActivity(intent);
            }
        });


        water.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(UserCategoryActivity.this, ProductsByCategoriesActivity.class);
                intent.putExtra("category", "water");
                startActivity(intent);
            }
        });


        household_chemicals.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(UserCategoryActivity.this, ProductsByCategoriesActivity.class);
                intent.putExtra("category", "household_chemicals");
                startActivity(intent);
            }
        });


        animals_products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(UserCategoryActivity.this, ProductsByCategoriesActivity.class);
                intent.putExtra("category", "animals_products");
                startActivity(intent);
            }
        });


        frozen_products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(UserCategoryActivity.this, ProductsByCategoriesActivity.class);
                intent.putExtra("category", "frozen_products");
                startActivity(intent);
            }
        });


        sweet_products.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(UserCategoryActivity.this, ProductsByCategoriesActivity.class);
                intent.putExtra("category", "sweet_products");
                startActivity(intent);
            }
        });
    }
}