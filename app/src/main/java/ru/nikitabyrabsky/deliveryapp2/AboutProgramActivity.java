package ru.nikitabyrabsky.deliveryapp2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Array;

public class AboutProgramActivity extends AppCompatActivity
{

    private TextView programTitle, programInfo, programContact;
    private ImageView programLogo;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_program);


        programContact = (TextView)findViewById(R.id.program_contact);
        programInfo = (TextView)findViewById(R.id.program_info);
        programTitle = (TextView)findViewById(R.id.program_title);
        programLogo = (ImageView)findViewById(R.id.program_logo);


    }
}