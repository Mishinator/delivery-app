package ru.nikitabyrabsky.deliveryapp2;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.HashMap;
import java.util.Set;

import de.hdodenhof.circleimageview.CircleImageView;
import ru.nikitabyrabsky.deliveryapp2.Prevalant.Prevalent;

public class SettingsActivity extends AppCompatActivity
{
    private CircleImageView profileImageView;
    private EditText fullNameEditText, userPhoneEditText, addressEditText, emailAddressEditText;
    private TextView profileChangeTextBtn, closeTextBtn, saveTextButton;

    private TextView userSex;
    private Button manUser, womanUser;

    private Uri imageUri;
    private String myUrl = "";
    private StorageTask uploadTask;
    private StorageReference storageProfilePictureRef;
    private String checker = "";





    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        storageProfilePictureRef = FirebaseStorage.getInstance().getReference().child("Profile pictures");

        profileImageView = (CircleImageView)findViewById(R.id.settings_profile_image);
        fullNameEditText = (EditText)findViewById(R.id.settings_full_name);
        userPhoneEditText = (EditText)findViewById(R.id.settings_phone_number);
        addressEditText = (EditText)findViewById(R.id.settings_address);
        emailAddressEditText = (EditText)findViewById(R.id.settings_email);
        profileChangeTextBtn = (TextView)findViewById(R.id.profile_image_change_btn);
        closeTextBtn = (TextView)findViewById(R.id.close_settings_btn);
        saveTextButton = (TextView)findViewById(R.id.update_settings_btn);
        userSex = (TextView)findViewById(R.id.settings_sex);
        manUser = (Button)findViewById(R.id.man);
        womanUser = (Button)findViewById(R.id.woman);

//        sexUser = (ListView)findViewById(R.id.sexUser);
//        sexUser.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
//                this, R.array.names,
//                android.R.layout.simple_list_item_single_choice);
//        sexUser.setAdapter(adapter);
//        names = getResources().getStringArray(R.array.names);


        userInfoDisplay(profileImageView, fullNameEditText, userPhoneEditText, addressEditText,emailAddressEditText);

        closeTextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                finish();

            }
        });


        saveTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(checker.equals("clicked"))
                {
                    userInfoSaved();
                }
                else
                {
                    updateOnlyUserInfo();
                }
            }
        });

        profileChangeTextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                checker = "clicked";

                CropImage.activity(imageUri)
                        .setAspectRatio(1, 1)
                        .start(SettingsActivity.this);


            }
        });


        manUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                userSex.setText("man");
                manUser.setBackgroundResource(R.color.cash);
                womanUser.setBackgroundResource(R.color.terminal);
            }
        });

        womanUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                userSex.setText("woman");
                womanUser.setBackgroundResource(R.color.cash);
                manUser.setBackgroundResource(R.color.terminal);
            }
        });
    }



    private void updateOnlyUserInfo()
    {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Users");

        HashMap<String, Object> userMap = new HashMap<>();
        userMap.put("name", fullNameEditText.getText().toString());
        userMap.put("address", addressEditText.getText().toString());
        userMap.put("phoneOrder", userPhoneEditText.getText().toString());
        userMap.put("email", emailAddressEditText.getText().toString());
        userMap.put("sex", userSex.getText().toString());
        ref.child(Prevalent.currentOnlineUser.getPhone()).updateChildren(userMap);

        startActivity(new Intent(SettingsActivity.this, MenuActivity.class));
        Toast.makeText(SettingsActivity.this, "Данные успешно обновлены!", Toast.LENGTH_SHORT).show();
        finish();

    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode==RESULT_OK && data!=null)
        {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            imageUri = result.getUri();
            
            profileImageView.setImageURI(imageUri);
        }
        else
        {
            Toast.makeText(this, "Ошибка, попробуйте снова", Toast.LENGTH_SHORT).show();

            startActivity(new Intent(SettingsActivity.this, SettingsActivity.class));
            finish();
        }
    }

    

    private void userInfoSaved()
    {
        if (TextUtils.isEmpty(fullNameEditText.getText().toString()))
        {
            Toast.makeText(this, "Имя является обязательным", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(addressEditText.getText().toString()))
        {
            Toast.makeText(this, "Адрес является обязательным", Toast.LENGTH_SHORT).show();
        }
        else if (TextUtils.isEmpty(userPhoneEditText.getText().toString()))
        {
            Toast.makeText(this, "Телефон является обязательным", Toast.LENGTH_SHORT).show();
        }
        else if(checker.equals("clicked"))
        {
            uploadImage();
        }
    }

    private void uploadImage()
    {

        if(imageUri != null)
        {


            final StorageReference fileRef = storageProfilePictureRef
                    .child(Prevalent.currentOnlineUser.getPhone() + ".jpg");

            uploadTask = fileRef.putFile(imageUri);

            uploadTask.continueWithTask(new Continuation() {
                @Override
                public Object then(@NonNull Task task) throws Exception
                {

                    if(!task.isSuccessful())
                    {
                        throw task.getException();
                    }

                    return fileRef.getDownloadUrl();
                }
            })

            .addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task)
                {
                    if(task.isSuccessful())
                    {
                        Uri downloadUrl = task.getResult();

                        myUrl = downloadUrl.toString();

                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Users");

                        HashMap<String, Object> userMap = new HashMap<>();
                        userMap.put("name", fullNameEditText.getText().toString());
                        userMap.put("address", addressEditText.getText().toString());
                        userMap.put("phoneOrder", userPhoneEditText.getText().toString());
                        userMap.put("email", emailAddressEditText.getText().toString());
                        userMap.put("sex", userSex.getText().toString());
                        userMap.put("image", myUrl);
                        ref.child(Prevalent.currentOnlineUser.getPhone()).updateChildren(userMap);

                        startActivity(new Intent(SettingsActivity.this, MenuActivity.class));
                        Toast.makeText(SettingsActivity.this, "Данные успешно обновлены!", Toast.LENGTH_SHORT).show();
                        finish();


                    }
                    else 
                    {
                        Toast.makeText(SettingsActivity.this, "Ошибка...", Toast.LENGTH_SHORT).show();

                    }

                }
            });
        }
        else
         {
             Toast.makeText(this, "Изображение не выбрано", Toast.LENGTH_SHORT).show();

         }
    }


    private void userInfoDisplay(final CircleImageView profileImageView, final EditText fullNameEditText, final EditText userPhoneEditText, final EditText addressEditText, final EditText emailAddressEditText)
    {


        DatabaseReference UserRef = FirebaseDatabase.getInstance().getReference().child("Users").child(Prevalent.currentOnlineUser.getPhone());

        UserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot)
            {
                if(snapshot.exists())
                {
                    if(snapshot.child("image").exists())
                    {
                        String image = snapshot.child("image").getValue().toString();
                        String name = snapshot.child("name").getValue().toString();
                        String phone = snapshot.child("phone").getValue().toString();
                        String address = snapshot.child("address").getValue().toString();
                        String email = snapshot.child("email").getValue().toString();
                        String sex = snapshot.child("sex").getValue().toString();

                        Picasso.get().load(image).into(profileImageView);
                        fullNameEditText.setText(name);
                        userPhoneEditText.setText(phone);
                        addressEditText.setText(address);
                        emailAddressEditText.setText(email);
                        userSex.setText(sex);
                    }
                }
            }


            @Override
            public void onCancelled(@NonNull DatabaseError error)
            {

            }
        });
    }
}