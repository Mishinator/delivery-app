package ru.nikitabyrabsky.deliveryapp2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class CourierActivity extends AppCompatActivity
{

    private Button courierCheckOrders, courierLogoutBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courier);


        courierCheckOrders = (Button)findViewById(R.id.courier_check_orders_btn);
        courierLogoutBtn = (Button)findViewById(R.id.courier_logout_btn);

        courierCheckOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(CourierActivity.this, AdminNewOrdersActivity.class);
                startActivity(intent);

            }
        });

        courierLogoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(CourierActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
    }
}