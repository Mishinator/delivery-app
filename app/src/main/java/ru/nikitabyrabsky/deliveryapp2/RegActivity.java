package ru.nikitabyrabsky.deliveryapp2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegActivity extends AppCompatActivity {

    private Button CreateAccountButton;
    private EditText InputName, InputPhone, InputPassword;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg);

        CreateAccountButton = (Button)findViewById(R.id.reg_btn);
        InputName = (EditText)findViewById(R.id.reg_name_input);
        InputPhone = (EditText) findViewById(R.id.reg_phone_input);
        InputPassword = (EditText)findViewById(R.id.reg_password_input);

        progressBar = (ProgressBar)findViewById(R.id.progressBar1);


        CreateAccountButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                CreateAccount();
            }

            private void CreateAccount()
            {
                String name = InputName.getText().toString();
                String phone = InputPhone.getText().toString();
                String password = InputPassword.getText().toString();

                boolean pw = false;
                if(password.matches("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s).*$"))
                {
                    pw = true;
                }







                if(TextUtils.isEmpty(name))
                {
                    Toast.makeText(RegActivity.this, "Введите имя....", Toast.LENGTH_SHORT).show();
                }

                else if(TextUtils.isEmpty(phone))
                {
                    Toast.makeText(RegActivity.this, "Введите номер....", Toast.LENGTH_SHORT).show();
                }
                else if (phone.length() > 10 || phone.length() < 10)
                {
                    Toast.makeText(RegActivity.this, "Введенный номер не соответствует телефонному номеру", Toast.LENGTH_SHORT).show();
                }

                else if(TextUtils.isEmpty(password))
                {
                    Toast.makeText(RegActivity.this, "Введите пароль...", Toast.LENGTH_SHORT).show();
                }
                else if(pw == false || password.length() < 8)
                {
                    Toast.makeText(RegActivity.this, "Пароль не удовлетворяет требования сложности.", Toast.LENGTH_SHORT).show();
                    Toast.makeText(RegActivity.this, "Строчные и прописные латинские буквы, цифры. Минимум 8 символов", Toast.LENGTH_LONG).show();

                }


                else
                    {
                        ValidateEmailAddress(name, phone, password);
                    }

            }

            private void ValidateEmailAddress(final String name, final String phone, final String password)
            {
                progressBar.setVisibility(ProgressBar.VISIBLE);

                final DatabaseReference RootRef;
                RootRef = FirebaseDatabase.getInstance().getReference();

                RootRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                    {
                        if(!(dataSnapshot.child("Users").child(phone).exists()))
                        {//если email не используется
                            HashMap<String, Object> userdataMap = new HashMap<>();
                            userdataMap.put("phone", phone);
                            userdataMap.put("password", password);
                            userdataMap.put("name", name);

                            RootRef.child("Users").child(phone).updateChildren(userdataMap)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task)
                                        {
                                            if( task.isSuccessful())
                                            {
                                                Toast.makeText(RegActivity.this, "Аккаунт создан!", Toast.LENGTH_SHORT).show();

                                                Intent intent = new Intent(RegActivity.this, LoginActivity.class );
                                                startActivity(intent);
                                                progressBar.setVisibility(ProgressBar.INVISIBLE);
                                            }
                                            else
                                            {
                                                Toast.makeText(RegActivity.this, "Ошибка, повторите снова!", Toast.LENGTH_SHORT).show();
                                                progressBar.setVisibility(ProgressBar.INVISIBLE);
                                            }
                                        }
                                    });

                        }
                        else
                        {//если телефон уже используется
                            Toast.makeText(RegActivity.this, "Этот телефон " + phone + " уже используется", Toast.LENGTH_SHORT).show();
                            Toast.makeText(RegActivity.this, "Используйте другой номер для регистрации", Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(RegActivity.this, MainActivity.class );
                            startActivity(intent);
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
            }
        });
    }
}