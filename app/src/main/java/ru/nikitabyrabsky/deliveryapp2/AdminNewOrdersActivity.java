package ru.nikitabyrabsky.deliveryapp2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ru.nikitabyrabsky.deliveryapp2.Model.AdminOrders;

public class AdminNewOrdersActivity extends AppCompatActivity
{

    private RecyclerView ordersList;
    private DatabaseReference ordersRef;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_new_orders);


        ordersRef = FirebaseDatabase.getInstance().getReference().child("Orders");


        ordersList = findViewById(R.id.orders_list);
        ordersList.setLayoutManager(new LinearLayoutManager(this));




    }


    @Override
    protected void onStart()
    {
        super.onStart();




        FirebaseRecyclerOptions<AdminOrders> options =
                new FirebaseRecyclerOptions.Builder<AdminOrders>()
                .setQuery(ordersRef, AdminOrders.class)
                .build();

        FirebaseRecyclerAdapter<AdminOrders, AdminOrdersViewHolder> adapter =
                new FirebaseRecyclerAdapter<AdminOrders, AdminOrdersViewHolder>(options) {
                    @Override
                    protected void onBindViewHolder(@NonNull AdminOrdersViewHolder holder, final int position, @NonNull final AdminOrders model)
                    {
                        holder.userName.setText("Имя: " + model.getUser_name());
                        holder.userPhoneNumber.setText("Номер телефона: " + model.getPhone());
                        holder.userTotalPrice.setText("Итоговая стоимость: " + model.getTotalAmount());
                        holder.userDeliveryAddress.setText("Адрес доставки: " + model.getAddress());
                        holder.userDateTime.setText("Доставить : " + model.getDelivery_date() + " к " + model.getDelivery_time());
                        holder.userWayPay.setText("Способ оплаты: " + model.getPayway());

                        holder.ShowOrdersBtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                                String uID = getRef(position).getKey();

                                Intent intent = new Intent(AdminNewOrdersActivity.this, AdminUserProductsActivity.class);
                                intent.putExtra("uid", uID);
                                startActivity(intent);
                            }
                        });


                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                                CharSequence options[] = new CharSequence[]
                                        {
                                                "Да",
                                                "Нет"
                                        };

                                AlertDialog.Builder builder = new AlertDialog.Builder(AdminNewOrdersActivity.this);
                                builder.setTitle("Данный заказ был доставлен?");

                                builder.setItems(options, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int i)
                                    {
                                        if (i == 0)//кнопка ДА
                                        {
                                            String uID = getRef(position).getKey();

                                            RemoverOrder(uID);
                                        }
                                        else//кнопка НЕТ
                                        {
                                            finish();
                                        }

                                    }
                                });
                                builder.show();
                            }
                        });
                    }

                    @NonNull
                    @Override
                    public AdminOrdersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
                    {
                        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.orders_layout, parent, false);
                        return new AdminOrdersViewHolder(view);
                    }
                };

        ordersList.setAdapter(adapter);
        adapter.startListening();
    }




    public static class AdminOrdersViewHolder extends RecyclerView.ViewHolder
    {
        public TextView userName, userPhoneNumber, userTotalPrice, userDateTime, userDeliveryAddress, userWayPay;
        public Button ShowOrdersBtn;
        public Button stage1, stage2, stage3, stage4;

        public AdminOrdersViewHolder(@NonNull View itemView)
        {
            super(itemView);


            userName = itemView.findViewById(R.id.order_user_name);
            userPhoneNumber = itemView.findViewById(R.id.order_phone_number);
            userTotalPrice = itemView.findViewById(R.id.order_total_price);
            userDateTime = itemView.findViewById(R.id.order_date_time);
            userDeliveryAddress = itemView.findViewById(R.id.order_address_city);
            ShowOrdersBtn = itemView.findViewById(R.id.show_all_products_btn);
            stage1 = itemView.findViewById(R.id.stage_1);
            stage2 = itemView.findViewById(R.id.stage_2);
            stage3 = itemView.findViewById(R.id.stage_3);
            stage4 = itemView.findViewById(R.id.stage_4);
            userWayPay = itemView.findViewById(R.id.way_pay);




            stage1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    stage1.setBackgroundResource(R.color.yes);
                    stage2.setVisibility(View.VISIBLE);
                    stage2.setBackgroundResource(R.color.no);
                    v.setClickable(false);

                }
            });

            stage2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    stage2.setBackgroundResource(R.color.yes);
                    stage3.setVisibility(View.VISIBLE);
                    stage3.setBackgroundResource(R.color.no);
                    v.setClickable(false);

                }
            });

            stage3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    stage3.setBackgroundResource(R.color.yes);
                    stage4.setVisibility(View.VISIBLE);
                    stage4.setBackgroundResource(R.color.no);
                    v.setClickable(false);

                }
            });

            stage4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    stage4.setBackgroundResource(R.color.yes);
                    v.setClickable(false);

                }
            });

        }
    }


    private void RemoverOrder(String uID)
    {
        ordersRef.child(uID).removeValue();
    }
}