package ru.nikitabyrabsky.deliveryapp2.Model;

public class AdminOrders
{
    private String user_name, phone, address, city, totalAmount, state, delivery_date, delivery_time, payway;

    public AdminOrders()
    {

    }

    public AdminOrders(String user_name, String phone, String address, String city, String totalAmount, String state, String delivery_date, String delivery_time, String payway) {
        this.user_name = user_name;
        this.phone = phone;
        this.address = address;
        this.city = city;
        this.totalAmount = totalAmount;
        this.state = state;
        this.delivery_date = delivery_date;
        this.delivery_time = delivery_time;
        this.payway = payway;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDelivery_date() {
        return delivery_date;
    }

    public void setDelivery_date(String delivery_date) {
        this.delivery_date = delivery_date;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getPayway() {
        return payway;
    }

    public void setPayway(String payway) {
        this.payway = payway;
    }
}
