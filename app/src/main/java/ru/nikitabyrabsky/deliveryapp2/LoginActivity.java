package ru.nikitabyrabsky.deliveryapp2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import io.paperdb.Paper;
import ru.nikitabyrabsky.deliveryapp2.Model.User;
import ru.nikitabyrabsky.deliveryapp2.Prevalant.Prevalent;

public class LoginActivity extends AppCompatActivity {

    private EditText InputPhone, InputPassword;
    private Button LoginButton;

    private TextView AdminLink, NotAdminLink;
    private TextView CourierLink, NotCourierLink;

    private String parentDbName = "Users";
    private CheckBox chkBoxRememberMe;

    private ProgressBar progressBar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        LoginButton = (Button) findViewById(R.id.login_btn);
        InputPhone = (EditText) findViewById(R.id.login_phone_input);
        InputPassword = (EditText) findViewById(R.id.login_password_input);

        AdminLink = (TextView)findViewById(R.id.admin_panel_link);
        NotAdminLink = (TextView)findViewById(R.id.not_admin_panel_link);
        CourierLink = (TextView)findViewById(R.id.courier_panel_link);
        NotCourierLink = (TextView)findViewById(R.id.not_courier_panel_link);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);


        chkBoxRememberMe = (CheckBox)findViewById(R.id.remember_me_chkB);
        Paper.init(this);

        LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                LoginUser();
            }
        });

        AdminLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                LoginButton.setText("Войти как администратор");
                AdminLink.setVisibility(View.INVISIBLE);
                NotAdminLink.setVisibility(View.VISIBLE);
                CourierLink.setVisibility(View.INVISIBLE);
                chkBoxRememberMe.setVisibility(View.INVISIBLE);
                parentDbName = "Admins";
            }
        });

        NotAdminLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                LoginButton.setText("Войти");
                AdminLink.setVisibility(View.VISIBLE);
                NotAdminLink.setVisibility(View.INVISIBLE);
                CourierLink.setVisibility(View.VISIBLE);
                chkBoxRememberMe.setVisibility(View.VISIBLE);
                parentDbName = "Users";
            }
        });

        CourierLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                LoginButton.setText("Войти как курьер");
                CourierLink.setVisibility(View.INVISIBLE);
                NotCourierLink.setVisibility(View.VISIBLE);
                AdminLink.setVisibility(View.INVISIBLE);
                chkBoxRememberMe.setVisibility(View.INVISIBLE);
                parentDbName = "Couriers";
            }
        });

        NotCourierLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                LoginButton.setText("Войти");
                CourierLink.setVisibility(View.VISIBLE);
                NotCourierLink.setVisibility(View.INVISIBLE);
                AdminLink.setVisibility(View.VISIBLE);
                chkBoxRememberMe.setVisibility(View.VISIBLE);
                parentDbName = "Users";
            }
        });


    }

    private void LoginUser()
    {
        String phone = InputPhone.getText().toString();
        String password = InputPassword.getText().toString();

        if(TextUtils.isEmpty(phone))
        {
           Toast.makeText(this, "Введите номер....", Toast.LENGTH_SHORT).show();
        }

        else if(TextUtils.isEmpty(password))
        {
            Toast.makeText(this, "Введите пароль...", Toast.LENGTH_SHORT).show();
        }
        else
        {
            AllowAccessToAccount(phone, password);
        }
    }

    private void AllowAccessToAccount(final String phone, final String password)
    {
        if(chkBoxRememberMe.isChecked())
        {
            Paper.book().write(Prevalent.UserPhoneKey, phone);
            Paper.book().write(Prevalent.UserPasswordKey, password);
        }

        progressBar.setVisibility(ProgressBar.VISIBLE);

        final DatabaseReference RootRef;
        RootRef = FirebaseDatabase.getInstance().getReference();


        RootRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot)
            {
                if(snapshot.child(parentDbName).child(phone).exists())
                {
                    User userData = snapshot.child(parentDbName).child(phone).getValue(User.class);

                    if(userData.getPhone().equals(phone))
                    {
                        if(userData.getPassword().equals(password))
                        {
                            if(parentDbName.equals("Admins"))
                            {
                                Toast.makeText(LoginActivity.this, "Добро пожаловать, администратор!", Toast.LENGTH_SHORT).show();

                                progressBar.setVisibility(ProgressBar.INVISIBLE);
                                Intent intent = new Intent(LoginActivity.this, AdminCategoryActivity.class );
                                startActivity(intent);
                            }
                            else if(parentDbName.equals("Couriers"))
                            {
                                Toast.makeText(LoginActivity.this, "Вход выполнен успешно!", Toast.LENGTH_SHORT).show();

                                progressBar.setVisibility(ProgressBar.INVISIBLE);
                                Intent intent = new Intent(LoginActivity.this, CourierActivity.class );
                                startActivity(intent);
                            }
                            else if(parentDbName.equals("Users"))
                            {
                                Toast.makeText(LoginActivity.this, "Вход выполнен успешно", Toast.LENGTH_SHORT).show();

                                progressBar.setVisibility(ProgressBar.INVISIBLE);
                                Intent intent = new Intent(LoginActivity.this, MenuActivity.class );
                                Prevalent.currentOnlineUser = userData;
                                startActivity(intent);


                            }
                        }
                        else
                        {
                            Toast.makeText(LoginActivity.this, "Неверный логин или пароль", Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                        }
                    }
                }
                else
                {
                    Toast.makeText(LoginActivity.this, "Аккаунт с этим номером телефона не существует", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(ProgressBar.INVISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error)
            {

            }
        });
    }
}